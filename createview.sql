CREATE VIEW Registration AS
	SELECT
	Student.StudentID, Student.StudentName, Student.StudentSurname, Courses.CourseName, Courses.Credit, Regist.Semester
	FROM Regist INNER JOIN Student ON Regist.StudentID = Student.StudentID
	INNER JOIN Courses ON Regist.CourseID = Courses.CourseID; 
		
